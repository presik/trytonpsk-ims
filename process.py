# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction

__all__ = ['Process']


class Process(ModelSQL, ModelView):
    "Process"
    __name__ = "ims.process"
    name = fields.Char('Name', required=True, translate=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @classmethod
    def __setup__(cls):
        super(Process, cls).__setup__()

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False
