# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .document_register import (DocumentRegister, DocumentRegisterHistory)
from .configuration import Configuration
from .nonconformity import (NonConformity, NonConformityReport, NonConformityCategory)
from .improvement import (Improvement, ImprovementReport, ActionLine)
from .process import Process
from .report import (IsoReport, PurchaseReport, MedicalExamReport,
    UniformReport, FreeLiabilityReport, RequestServiceReport)


def register():
    Pool.register(
        Process,
        NonConformityCategory,
        Improvement,
        ActionLine,
        NonConformity,
        Configuration,
        DocumentRegister,
        DocumentRegisterHistory,
        module='ims', type_='model')
    Pool.register(
        NonConformityReport,
        ImprovementReport,
        IsoReport,
        PurchaseReport,
        UniformReport,
        MedicalExamReport,
        FreeLiabilityReport,
        RequestServiceReport,
        module='ims', type_='report')
