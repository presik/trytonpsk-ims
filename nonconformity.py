# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from datetime import date
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['NonConformity', 'NonConformityReport', 'NonConformityCategory']

STATES = {
    'readonly': (Eval('state') != 'draft'),
}

STATES_OPEN = {
    'readonly': (Eval('state') != 'open'),
    'required': (Eval('state') == 'closed'),
}

STATES_CLOSE = {
    'readonly': (Eval('state') != 'open'),
    'required': (Eval('state') == 'closed'),
}


class NonConformity(Workflow, ModelSQL, ModelView):
    'Non Conformity'
    __name__ = 'ims.nonconformity'
    # _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    reported_by = fields.Many2One('party.party',
        'Reported By', states=STATES)
    effective_date = fields.Date('Effective Date',
        states=STATES_OPEN)
    process = fields.Many2One('ims.process', 'Process', states=STATES)
    source = fields.Selection([
        ('other', 'Other'),
        ('internal_audit', 'Internal Audit'),
        ('external_audit', 'External Audit'),
        ('legal', 'Legal'),
        ('complain', 'Complain'),
        ], 'Source', states=STATES)
    source_string = source.translated('source')
    kind = fields.Selection([
        ('critical', 'Critical'),
        ('major', 'Major'),
        ('minor', 'Minor'),
        ], 'Kind', states=STATES)
    kind_string = kind.translated('kind')
    description = fields.Text('Description', required=True,
        states=STATES)
    cause = fields.Text('Cause', states=STATES_OPEN)
    immediate_action = fields.Text('Immediate Action',
        states=STATES_OPEN)
    improvement = fields.Many2One('ims.improvement', 'Improvement',
        states=STATES)
    close_date = fields.Date('Close Date',
        states={
            'readonly': Eval('state') != 'open',
            'required': Eval('state') == 'closed',
            }, depends=['state'])
    control_date = fields.Date('Control Date',
        states=STATES_CLOSE)
    responsible_employee = fields.Many2One('company.employee',
        'Responsible', states=STATES_CLOSE)
    company = fields.Many2One('company.company', 'Company',
        required=True, states=STATES, domain=[ ('id', If(In('company',
        Eval('context', {})), '=', '!='), Get(Eval('context', {}),
        'company', 0)), ])
    notes = fields.Text('Notes', states=STATES_CLOSE)
    improvement_state = fields.Selection([
            ('none', 'None'),
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed'),
            ('cancelled', 'Cancelled'),
        ], 'Improvement State', readonly=True, required=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed'),
            ('cancelled', 'Cancelled'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')

    @classmethod
    def __setup__(cls):
        super(NonConformity, cls).__setup__()
        cls._order.insert(0, ('create_date', 'DESC'))
        cls._order.insert(1, ('id', 'DESC'))
        # cls._error_messages.update({
        #     'missing_sequence_nonconformity': ('The sequence for '
        #         'non conformities is missing!'),
        # })
        cls._transitions |= set((
                ('draft', 'open'),
                ('open', 'closed'),
                ('open', 'draft'),
                ('closed', 'open'),
                ('open', 'cancelled'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': True,
                },
            'cancel': {
                'invisible': Eval('state') != 'open',
                },
            'open': {
                'invisible': Eval('state') != 'draft',
                },
            'close': {
                'invisible': Eval('state') != 'open',
                },
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_improvement_state():
        return 'none'

    @staticmethod
    def default_effective_date():
        return date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        for record in records:
            record.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, records):
        for record in records:
            pass

    def set_number(self):
        '''
        Set sequence number
        '''
        pool = Pool()
        config = pool.get('ims.configuration')(1)
        Sequence = pool.get('ir.sequence')
        if not config.nonconformity_sequence:
            self.raise_user_error('missing_sequence_nonconformity')

        seq_id = config.nonconformity_sequence.id
        self.write([self], {'number': Sequence.get_id(seq_id)})


class NonConformityReport(Report):
    __name__ = 'ims.nonconformity_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(NonConformityReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class NonConformityCategory(ModelSQL, ModelView):
    "Non Conformity Category"
    __name__ = 'ims.nonconformity.category'
    name = fields.Char('Name', required=True)
