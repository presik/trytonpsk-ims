# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from trytond.pool import Pool, PoolMeta
from trytond.report import Report

__all__ = ['IsoReport', 'PurchaseReport', 'MedicalExamReport',
        'UniformReport', 'FreeLiabilityReport', 'RequestServiceReport']


class IsoReport(Report):

    @classmethod
    def get_context(cls, records, data):
        report_context = super(IsoReport, cls).get_context(records, data)
        docrec = cls._get_report_ims_data()
        report_context['ims_report'] = docrec
        return report_context

    @classmethod
    def _get_report_ims_data(cls):
        DocRec = Pool().get('ims.document_register')
        docs_recs = DocRec.search([
            ('report.report_name', '=', cls.__name__),
        ])
        if docs_recs:
            return docs_recs[0]


class PurchaseReport(IsoReport):
    __metaclass__ = PoolMeta
    __name__ = 'purchase.purchase'


class UniformReport(IsoReport):
    __metaclass__ = PoolMeta
    __name__ = 'staff_contracting.uniform_report'


class MedicalExamReport(IsoReport):
    __metaclass__ = PoolMeta
    __name__ = 'staff_contracting.medical_exam_report'


class FreeLiabilityReport(IsoReport):
    __metaclass__ = PoolMeta
    __name__ = 'staff_contracting.free_liability_report'


class RequestServiceReport(IsoReport):
    __metaclass__ = PoolMeta
    __name__ = 'maintenance.request_service'
