# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from datetime import date
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['Improvement', 'ImprovementReport', 'ActionLine']

STATES = {
    'readonly': (Eval('state') != 'draft'),
}

STATES_OPEN = {
    'readonly': (Eval('state') != 'open'),
    'required': (Eval('state') == 'closed'),
}

STATES_CLOSE = {
    'readonly': (Eval('state') != 'open'),
    'required': (Eval('state') == 'closed'),
}


class Improvement(Workflow, ModelSQL, ModelView):
    'Improvement'
    __name__ = 'ims.improvement'
    # _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    process = fields.Many2One('ims.process', 'Process', required=True,
        states=STATES)
    improvement_date = fields.Date('Improvement Date')
    reported_by = fields.Many2One('party.party',
        'Reported By', states=STATES)
    source = fields.Selection([
            ('nonconformity', 'Non Conformity'),
            ('audit', 'Audit'),
            ('meeting', 'Meeting'),
            ('other', 'Other'),
        ], 'Source', states=STATES)
    source_string = source.translated('source')
    source_detail = fields.Char('Source Detail', states=STATES)
    kind = fields.Selection([
            ('project', 'Project'),
            ('plan', 'Plan'),
            ('corrective_action', 'Corrective Action'),
            ('preventive_action', 'Preventive Action'),
        ], 'Kind', states=STATES)
    kind_string = kind.translated('kind')
    description = fields.Text('Description', required=True, states=STATES)
    cause = fields.Text('Cause', states=STATES)
    close_date = fields.Date('Close Date', states=STATES_CLOSE)
    responsible_employee = fields.Many2One('company.employee',
            'Responsible', states=STATES_CLOSE)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, domain=[ ('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0)), ])
    actions = fields.One2Many('ims.improvement.action',
        'improvement', 'Action Lines')
    satisfactory = fields.Selection([
            ('', ''),
            ('yes', 'Yes'),
            ('no', 'No'),
            ], 'Satisfactory', states=STATES_CLOSE)
    satisfactory_string = satisfactory.translated('satisfactory')
    notes = fields.Text('Notes', states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed'),
            ('cancelled', 'Cancelled'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    previous_situation = fields.Text('Previous Situation', states=STATES_CLOSE)
    later_situation = fields.Text('Later Situation', states=STATES_CLOSE)

    @classmethod
    def __setup__(cls):
        super(Improvement, cls).__setup__()
        cls._order.insert(0, ('create_date', 'DESC'))
        cls._order.insert(1, ('id', 'DESC'))
        # cls._error_messages.update({
        #     'missing_sequence_improvement': ('The sequence for '
        #         'improvement is missing!'),
        # })
        cls._transitions |= set((
                ('draft', 'open'),
                ('open', 'closed'),
                ('closed', 'open'),
                ('open', 'cancelled'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': True,
                },
            'cancel': {
                'invisible': Eval('state') != 'open',
                },
            'open': {
                'invisible': Eval('state') == 'open',
                },
            'close': {
                'invisible': Eval('state') != 'open',
                },
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_close_date():
        return date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        for service in records:
            service.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, records):
        for service in records:
            pass

    def set_number(self):
        '''
        Set sequence number
        '''
        pool = Pool()
        config = pool.get('ims.configuration')(1)
        Sequence = pool.get('ir.sequence')
        if not config.improvement_sequence:
            self.raise_user_error('missing_sequence_improvement')

        seq_id = config.improvement_sequence.id
        self.write([self], {'number': Sequence.get_id(seq_id)})


class ImprovementReport(Report):
    __name__ = 'ims.improvement_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ImprovementReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class ActionLine(ModelSQL, ModelView):
    'Action Line'
    __name__ = 'ims.improvement.action'
    # _rec_name = 'sequence'
    improvement = fields.Many2One('ims.improvement',
        'improvement', required=True)
    sequence = fields.Integer('Sequence', required=True)
    action = fields.Text('Action')
    responsible = fields.Many2One('company.employee', 'Responsible')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date')
    revision_date = fields.Date('Revision Date')
    result = fields.Char('Result')
    revision_by = fields.Many2One('company.employee', 'Revision By')
