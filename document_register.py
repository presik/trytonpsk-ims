# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['DocumentRegister', 'DocumentRegisterHistory']


STATES = {
    'readonly': (Eval('state') != 'draft'),
}


class DocumentRegister(Workflow, ModelSQL, ModelView):
    'Document Register'
    __name__ = 'ims.document_register'
    # _rec_name = 'name'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', states=STATES)
    company = fields.Many2One('company.company', 'Company',
        required=True, states=STATES)
    report = fields.Many2One('ir.action.report', 'Report')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('released', 'Released'),
            ('inactive', 'Inactive'),
        ], 'State', readonly=True, required=True)
    kind = fields.Selection([
            ('document', 'Document'),
            ('register', 'Register'),
        ], 'Kind', required=True, states=STATES)
    history = fields.One2Many('ims.document_register.history',
            'document_register', 'History', states={
            'readonly': (Eval('state') == 'inactive'),
            })
    revision_date = fields.Function(fields.Date('Date'), 'get_history_data')
    revision = fields.Function(fields.Char('Revision'), 'get_history_data')

    @classmethod
    def __setup__(cls):
        super(DocumentRegister, cls).__setup__()
        # cls._error_messages.update({
        #     'missing_sequence_document_register': ('The sequence for '
        #         'customer service is missing!'),
        # })
        cls._transitions |= set((
                ('draft', 'released'),
                ('released', 'draft'),
                ('released', 'inactive'),
                ('inactive', 'released'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') != 'released',
                    },
                'inactive': {
                    'invisible': Eval('state') != 'released',
                    },
                'release': {
                    'invisible': Eval('state') == 'released',
                    },
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_kind():
        return 'register'

    @classmethod
    @ModelView.button
    @Workflow.transition('released')
    def release(cls, services):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('inactive')
    def inactive(cls, services):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, services):
        pass

    def get_history_data(self, name):
        res = []
        for ch in self.history:
            res.append(getattr(ch, name))
        return max(res)


class DocumentRegisterHistory(Workflow, ModelSQL, ModelView):
    'Document Register History'
    __name__ = 'ims.document_register.history'
    # _rec_name = 'sequence'
    sequence = fields.Integer('Sequence', required=True, )
    document_register = fields.Many2One('ims.document_register',
            'Document Register', required=True)
    revision_date = fields.Date('Revision Date', required=True)
    revision = fields.Char('Revision', required=True)
    approved_by = fields.Many2One('company.employee', 'Approved By',
            )
    created_by = fields.Many2One('company.employee', 'Created By',
            )
    description = fields.Text('Description', )
