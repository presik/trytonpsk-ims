# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'IMS Configuration'
    __name__ = 'ims.configuration'
    nonconformity_sequence = fields.Many2One('ir.sequence', 'Non Conformity Sequence',
        required=True)
    improvement_sequence = fields.Many2One('ir.sequence', 'Improvement Sequence', required=True)
